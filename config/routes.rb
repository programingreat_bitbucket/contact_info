# frozen_string_literal: true

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'
  # get 'contacts_error_log/index'
  resources :contact_errors, controller: 'contacts_error_log', only: :index
  resources :contacts, only: %i[index]

  # TODO: refactor this routes controller it can be better routes
  resources :contact_importer, only: %i[index new create]

  devise_for :users
  root 'contact_importer#new'
end
