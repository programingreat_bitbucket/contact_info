class AddStatusToContactInfoCsvs < ActiveRecord::Migration[6.1]
  def change
    add_column :contact_info_csvs, :status, :string
  end
end
