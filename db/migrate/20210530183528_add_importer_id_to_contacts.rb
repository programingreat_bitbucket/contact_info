class AddImporterIdToContacts < ActiveRecord::Migration[6.1]
  def change
    add_column :contacts, :source, :string
    add_reference :contacts, :contact_info_csv, foreign_key: true
  end
end
