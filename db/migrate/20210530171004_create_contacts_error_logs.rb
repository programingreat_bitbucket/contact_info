class CreateContactsErrorLogs < ActiveRecord::Migration[6.1]
  def change
    create_table :contacts_error_logs do |t|
      t.string :row, array: true
      t.string :log_message
      t.string :file_name
      t.string :date
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end

    add_index :contacts_error_logs, :row, using: 'gin'
  end
end
