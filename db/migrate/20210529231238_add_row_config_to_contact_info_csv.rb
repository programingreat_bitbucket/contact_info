class AddRowConfigToContactInfoCsv < ActiveRecord::Migration[6.1]
  def change
    default_obj = { name: 1, dob: 2, phone: 3, address: 4, cc: 5, franchise: 6, email: 7 }
    add_column :contact_info_csvs, :row_config, :jsonb, default: default_obj
    add_index  :contact_info_csvs, :row_config, using: :gin
  end
end
