class CreateContacts < ActiveRecord::Migration[6.1]
  def change
    create_table :contacts do |t|
      t.string :cc
      t.date :dob
      t.string :name
      t.string :email
      t.string :phone
      t.string :address
      t.string :franchise
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
  end
end
