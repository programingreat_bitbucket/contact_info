class CreateContactsWorker
  include Sidekiq::Worker

  def perform(csv_id)
    cinfo = ContactInfoCsv.find(csv_id)
    cinfo.mark_processing!

    rows = CSV.new(cinfo.csv.download).to_a
    row_config = cinfo.row_config

    rows.each do |row|
      Contact.create!(
        cc: row[row_config['cc']],
        dob: parse_date(row[row_config['dob']]),
        name: row[row_config['name']],
        email: row[row_config['email']],
        phone: row[row_config['phone']],
        address: row[row_config['address']],
        franchise: row[row_config['cc']].credit_card_brand_name,
        user_id: cinfo.user_id,
        contact_info_csv_id: cinfo.id
      )
    rescue StandardError => e
      attributes = { row: row, log_message: e.message, file_name: cinfo.csv.filename.to_s, user_id: cinfo.user_id }
      ContactsErrorLog.create(attributes)
    end

    if Contact.where(contact_info_csv_id: cinfo.id).count.zero? && rows.count > 1
      cinfo.mark_failed!
    else
      cinfo.mark_finished!
    end
  end

  def parse_date(date)
    return unless date[/\A(\d{8}|\d{4}-\d{2}-\d{2})\Z/]

    date.to_date
  end
end
