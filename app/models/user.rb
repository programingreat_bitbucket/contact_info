class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :rememberable

  has_many :contact_files, class_name: 'ContactInfoCsv', foreign_key: 'user_id'
  has_many :contacts
  has_many :contacts_error_logs
end
