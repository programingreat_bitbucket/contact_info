class Contact < ApplicationRecord
  belongs_to :user
  belongs_to :contact_info_csv, optional: true
  validates  :name, :dob, :phone, :address, :cc, :franchise, :email, presence: true
  validates :name, format: { with: /\A[A-Za-z\- ]+\Z/ }
  validates :phone, format: { with: /\(\+\d{2}\) \d{3}(-| )\d{3}(-| )\d{2}(-| )\d{2}\Z/ }
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :email, uniqueness: { scope: :user_id }
end
