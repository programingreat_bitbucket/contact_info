class ContactInfoCsv < ApplicationRecord
  include AASM

  aasm column: 'status' do
    state :on_hold, initial: true
    state :processing, :failed, :finished

    event :mark_processing do
      transitions from: %i[on_hold processing], to: :processing
    end

    event :mark_failed do
      transitions from: :processing, to: :failed
    end

    event :mark_finished do
      transitions from: :processing, to: :finished
    end
  end
  belongs_to :user
  has_one_attached :csv
  validates :csv, presence: true, blob: { content_type: /csv/i }
  store_accessor :row_config, :name, :dob, :phone, :address, :cc, :franchise, :email
  after_create :create_job
  before_create :row_config_adjustment

  # TODO: use translations
  ROW_CONFIG_NAME_MAPPING = {
    name: 'Name',
    dob: 'Date of Birth',
    phone: 'Phone Number',
    address: 'Address',
    cc: 'Credit Card',
    franchise: 'Franchise',
    email: 'Email'
  }.freeze

  PERMITED_PARAMS = ROW_CONFIG_NAME_MAPPING.keys.append(:csv).freeze

  private

  def row_config_adjustment
    row_config.transform_values! { |v| v.to_i - 1 }
  end

  def create_job
    CreateContactsWorker.perform_in(5.minutes, id)
  end
end
