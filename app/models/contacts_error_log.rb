class ContactsErrorLog < ApplicationRecord
  belongs_to :user

  NAME_MAPPING = {
    row: 'Row with errors',
    file_name: 'File',
    log_message: 'Error Message'
  }.freeze
end
