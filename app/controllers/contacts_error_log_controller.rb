class ContactsErrorLogController < ApplicationController
  before_action :authenticate_user!
  def index
    @pagy, @logs = pagy(current_user.contacts_error_logs, items: 5)
  end
end
