# frozen_string_literal: true

# Controller to handle all importer CSV
class ContactImporterController < ApplicationController
  before_action :authenticate_user!

  def new
    @contact_info = ContactInfoCsv.new
  end

  def index
    @pagy, @files = pagy(current_user.contact_files, items: 5)
  end

  def create
    current_user.contact_files.create!(permitted_contacts_params)
    redirect_to root_path, notice: 'File Uploaded!'
  rescue StandardError
    flash[:error] = I18n.t(:uplad_csv_error)
    redirect_to root_path
  end

  private

  def permitted_contacts_params
    params.require(:contact_info_csv).permit(ContactInfoCsv::PERMITED_PARAMS)
  end
end
